// feel free to change anything here when using in workshop
val circeVersion = "0.8.0"
val json4sVersion = "3.5.2"
val http4sVersion = "0.15.12"

name := "scala-workshop-todo-list-template"

version := "1.0"

scalaVersion := "2.12.1"

libraryDependencies ++= Seq(
// pick one json library or another, or both, or choose your own
  // json4s
  "org.json4s" %% "json4s-jackson" % json4sVersion,
  "org.http4s" %% "http4s-json4s-native" % http4sVersion,
  // circe
  "io.circe" %% "circe-core" % circeVersion,
  "io.circe" %% "circe-generic" % circeVersion,
  "io.circe" %% "circe-parser" % circeVersion,
  "org.http4s" %% "http4s-circe" % http4sVersion,
// http web server. Feel free to change this too if so inclined.
  "org.http4s" %% "http4s-dsl" % http4sVersion,
  "org.http4s" %% "http4s-blaze-server" % http4sVersion,
  "org.http4s" %% "http4s-blaze-client" % http4sVersion
)