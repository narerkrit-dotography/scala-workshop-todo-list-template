# Project requirements

Implement a backend server that stores a list of todo items

## Todo items
Each todo item should include

* title - a short title summarizing the task
* detail - free text field to provide additional information
* deadline (optional) - deadline of the task. can be a date or datetime
* status - whether the item is done or pending.

## Actions

users should be able to 

* List all unarchived items
* View a single item
* Create new item
* Modify an existing item
* Mark as done/pending
* Archive an item 
    * so that it doesn't show up in the list by default

# Optional requirements

If you have spare time, you could try to support these additional requirements.

## Handle and report failures

Respond with an appropriate status code and messages. Try not to use catching and throwing exceptions. Make it part of the application logic.

## Other actions

* Get only pending items
* Get only done items
* Get only archived items
* Sort results by
    * Pending/done status
    * Deadline
    * Title

