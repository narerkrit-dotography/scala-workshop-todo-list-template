package workshop.server

import org.http4s._
import org.http4s.dsl._

object TodoService {
  def service() = HttpService{
    case _ -> _ => NotFound("not implemented")
  }
}