package workshop.server

import scala.concurrent.duration._
import org.http4s.server.middleware.{CORS, CORSConfig}
import org.http4s._


object CorsMiddleware {

  private val corsConf = CORSConfig(
    anyOrigin = true,
    anyMethod = true,
    maxAge = 1.day.toSeconds,
    allowCredentials = false
  )

  def apply(service: HttpService) = CORS(service, corsConf)
}