package workshop.server

import scala.concurrent.{ Future, ExecutionContext }
import org.http4s._
import org.http4s.dsl._
import org.http4s.circe._

import io.circe.{ Decoder, Encoder }
import io.circe.syntax._

case class DummyClazz(str: String, i: Int)
object DummyClazz {
  implicit val dummyClazzDecoder = Decoder.forProduct2("leStr","leInt"){ DummyClazz.apply _ }
  implicit val dummyClazzEncoder = Encoder.forProduct2("leStr","leInt"){ DummyClazz.unapply _ andThen (_.get) }
}

object ExampleService {
  def service(implicit ec: ExecutionContext) = HttpService {
    case GET -> Root / "ping" => Ok("pong")
    case GET -> Root / "future" => Ok(Future.successful("futuristic"))
    case GET -> Root / "error" => ServiceUnavailable("dummy error")
    case GET -> Root / "echo" / "simple" / someString => Ok("got string: " + someString)
    case GET -> Root / "echo" / "long" / LongVar(long) => Ok("got long: " + long.toString)
    case req @ POST -> Root / "echo"/ "body" =>
      for {
        body <- req.as[String]
        resp <- Ok("got body: " + body)
      } yield resp
    case req @ POST -> Root / "json" =>
      for {
        dummyClazz <- req.as(jsonOf[DummyClazz])
        resp <- Ok(dummyClazz.toString)
      } yield resp
    case GET -> Root / "json" => Ok(DummyClazz("dummy",4321).asJson)
  }
}
