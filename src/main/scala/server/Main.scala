package workshop.server

import scala.concurrent.ExecutionContext.Implicits.global

import org.http4s.server.{Server, ServerApp}
import org.http4s.server.blaze._

object Main extends ServerApp{
  override def server(args: List[String]) = {
    println("server starting...")
    println("press Ctrl + C to stop the server")
    BlazeBuilder
      .bindHttp(8080,"localhost")
      .mountService(CorsMiddleware(ExampleService.service),"/ex")
      .mountService(CorsMiddleware(TodoService.service),"/api")
      .start
  }
}