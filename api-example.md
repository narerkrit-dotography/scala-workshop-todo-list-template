# API documentation: Example

these endpoints are the ones included in the template

### Ping

simple ping

*Path:* `GET /ex/ping`

*Response:* a string `pong`

### Future

like simple ping, but to show that the code also works with asynchronous implementation

*Path:* `GET /ex/future`
*Response:* a simple string

### Error

always error 503

*Path:* `GET /ex/error`
*Response:* the response will always be a code 503 with a dummy error string

### Echo simple

echo the path part

*Path:* `GET /ex/echo/simple/:s`
    * `:s` is any string
*Response:* print out the `:s`

### Echo long

echo the path part. Works only with `long` type number

*Path:* `GET /ex/echo/long/:l`
    * `:l` is any long number
*Response:* print out the number `:l`

### Echo body

echo the body string

*Path:* `POST /ex/echo/body`
    * the body can be any string
*Response:* print out the body

### json to class

This endpoint is to show how to convert a json into an instance of a class

*Path:* `POST /ex/json`

The body must be a json object in this format
```json
{
    "leStr": String
    "leInt": Int
}
```

*Response:* string representation of the class created from the input body

### class to json

This endpoints demonstrate how to convert a class into a json representation

*Path:* `GET /ex/json`
*Response:* a fixed JSON of same format as above