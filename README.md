# Scala workshop template for TODO List #

## Motivation

This repository is a template for the purpose of Scala training workshop. It contains a simple skeletal structure to get started with implementing a simple TODO list.

## Getting started

The template uses the library `http4s` to implement a HTTP server. Check the file `src/main/scala/server/ExampleService.scala` or the official documentation to see how to implement the service. You can check the result by starting the server and try sending it a request.

see the [Example API documentation](api-example.md) 

## Instructions and requirements

Implement your TODO list and wire it up to the TodoService in `src/main/scala/server/TodoService.scala`.

__see the requirements [here](REQUIREMENTS.md)__

**NOTE**: the todo service is prefixed with a `api` path fragment by default

## starting the server

simply execute the command `run` in the sbt terminal. press `ctrl + c` to stop the server.

## Documentations

head over to the official documentations for more info and examples

* **[http4s](http://http4s.org/v0.15)**
    * [scaladoc](http://http4s.org/v0.15/api/org/http4s/)
    * [github example](https://github.com/http4s/http4s/tree/master/examples/src/main/scala/com/example/http4s)
* **[json4s with example](http://json4s.org/)**
    * [github](https://github.com/json4s/json4s)
* **[circe](https://circe.github.io/circe/)**
    * [scaladoc](http://circe.github.io/circe/api/)
    * [codec example](http://circe.github.io/circe/codec.html)